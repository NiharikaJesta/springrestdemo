<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri = "http://www.springframework.org/tags/form"
 prefix = "form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	     <h2>Enter Product Information</h2>
      <form:form method = "POST" action = "addProduct" modelAttribute="prdData">
         <table>
            <tr>
               <td><form:label path = "productid">product Id</form:label></td>
               <td><form:input path = "productid" /></td>
            </tr>
            <tr>
               <td><form:label path = "productname">productName</form:label></td>
               <td><form:input path = "productname" /></td>
            </tr>
            <tr>
               <td><form:label path = "price">price</form:label></td>
               <td><form:input path = "price" /></td>
            </tr>
            <tr>
               <td><form:label path = "quantity">Quantity</form:label></td>
               <td><form:input path = "quantity" /></td>
            </tr>
            <tr>
               <td colspan = "2">
                  <input type = "submit" value = "Submit"/>
               </td>
            </tr>
         </table>  
      </form:form>
  
</body>
</html>