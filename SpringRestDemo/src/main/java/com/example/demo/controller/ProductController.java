package com.example.demo.controller;
import 	org.springframework.security.core.Authentication;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.dao.ProductDAO;
import com.example.demo.model.Product;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ApiResponse;
@RestController

public class ProductController {
	@Autowired
	ProductDAO pdao;

	 @ApiOperation(value = "Product List", notes =
	  "This method will retreive list from database", nickname = "getProducts")
	  
	  @ApiResponses(value = {
	  
	  @ApiResponse(code = 500, message = "Server error"),
	  
	  @ApiResponse(code = 404, message = "Product not found"),
	  
	  @ApiResponse(code = 200, message = "Successful retrieval", response =
	  Product.class, responseContainer = "List") })
	  
	 
	
	@GetMapping("/Retreive")
	public List<Product> getProductList() {
		List<Product> prdlist = pdao.getData();
		return prdlist;
	}

	@GetMapping("/RetreiveByID/{productid}")
	public Product getProductData(@PathVariable("productid") int prdid) {
		Product pdata = pdao.getById(prdid);
		return pdata;
	}

	@RequestMapping("/save")
	public ModelAndView saveDate() {
		System.out.println("coming in controller");
		return new ModelAndView("ProdctForm", "prdData", new Product());

	}

	@PostMapping("/addProduct")
	public ModelAndView display(@ModelAttribute("prdData") Product p1) {

		return new ModelAndView("AddProduct", "productdata", p1);
	}

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public ModelAndView login(String error, String logout) {
		System.out.println("Coming here ");
		if (error != null)
			return new ModelAndView("login", "Errormsg", "Your username and password are invalid.");

		if (logout != null)
			return new ModelAndView("login", "msg", "You have been logged out successfully.");
		return new ModelAndView("login");
	} 

	
	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public String logoutPage(HttpServletRequest request, HttpServletResponse response) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth != null) {
			new SecurityContextLogoutHandler().logout(request, response, auth);
		}
		return "redirect:/login";
	}

}