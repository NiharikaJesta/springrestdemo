package com.example.demo.dao;

import org.springframework.stereotype.Component;

import com.example.demo.model.Product;

import java.util.*;
@Component
public class ProductDAO {
	private List<Product> plist;
	public ProductDAO()
	{
		plist=new ArrayList<Product>();
		plist.add(new Product(101,"Laptop",90000,2));
		plist.add(new Product(102,"IPAD",80000,3));
	}
	public void save(Product p)
	{
		plist.add(p);
		System.out.println("Data saved");
	}
	public List<Product> getData()
	{
		return plist;
	}
	public Product getById(int productid)
	{
		Product pfind=null;
		for(Product p:plist)
		{
			if(p.getProductid()==productid)
			{
				pfind=p;
			}
			else
			{
				System.out.println("product not found");
			}
		}
		return pfind;
	}
	
	

}